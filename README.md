# bmed - Bookmark Editor for PDFs

A little tool to embed tables of contents ("bookmarks") into PDFs.

## Requirements
`bmed` uses the external program `pdftk` to import and export bookmark
metadata.

Ubuntu:
```
$ sudo apt install pdftk
```
Manjaro:
```
$ sudo pacman -S pdftk
```

## Usage
```
bmed <document.pdf> <bookmarks.txt>
```

The document must be in the PDF format. It may or may not already contain
bookmarks.

The bookmark file contains one bookmark entry per line. Entries have the
following format:
```<page> <bookmark level> <title>```
Example:
```
1   1   Introduction
12  1   Basics
12  2   Voltage and Current
43  2   Diodes and Transistors
85  3   Basic circuits
112 1   Operational Amplifiers
```
Empty lines are ignored.

The level of a bookmark is used to represent nested bookmarks ('1' is top
level,'2' is a subchapter, '3' is a sub-subchapter etc.).
